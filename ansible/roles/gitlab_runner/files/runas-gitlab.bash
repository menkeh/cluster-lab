#!/usr/bin/env bash

declare -A userid_mapping
userid_mapping["hmenke"]="1149493"

declare -A target_mapping
target_mapping["hmenke"]="alice"

if [[ "${userid_mapping["$1"]}" == "${JWT_USER_ID}" ]]; then
    echo "{\"username\":\"${target_mapping["$1"]}\"}"
    exit 0
fi

exit 1
